<?php


class Reponse
{
    private $id;
    private $reponse_id;
    private $value;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getReponseId()
    {
        return $this->reponse_id;
    }

    /**
     * @param mixed $reponse_id
     */
    public function setReponseId($reponse_id)
    {
        $this->reponse_id = $reponse_id;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }


}