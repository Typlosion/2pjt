<?php


interface Champs
{
    public function getChamps($id);

    public function getAllChamps();

    public function addChamps(Champs $Champs);

    public function updateChamps($Champs);

    public function deleteChamps($id);

}