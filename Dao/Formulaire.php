<?php


interface Formulaire
{
    public function getFormulaire();

    public function getAllFormulaire();

    public function addFormulaire();

    public function deleteFormulaire();

    public function updateFormulaire();
}