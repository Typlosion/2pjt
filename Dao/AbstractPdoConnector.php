<?php
/**
 * Created by PhpStorm.
 * User: sebastien
 * Date: 16/03/2018
 * Time: 11:31
 */

abstract class AbstractPdoConnector
{
    /**
     * @var \PDO
     */
    protected $pdo;

    public function __construct()
    {
        try {
            $this->pdo = new PDO("mysql:dbname=2pjt; host:localhost:3306", "root", "");
        } catch (\PDOException $e)
        {
            echo "Erreur de connexion à la base de données";
            die;
        }
    }
}